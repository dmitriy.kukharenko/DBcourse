namespace BD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccessLevels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        PositionId = c.Guid(nullable: false),
                        AccessLevelId = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Patronymic = c.String(),
                        DateRegistered = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccessLevels", t => t.AccessLevelId, cascadeDelete: true)
                .ForeignKey("dbo.Positions", t => t.PositionId, cascadeDelete: true)
                .Index(t => t.PositionId)
                .Index(t => t.AccessLevelId);
            
            CreateTable(
                "dbo.XrayPhotoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PhotoLink = c.String(),
                        Description = c.String(),
                        DateLoaded = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.XrayPhotoes", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.Users", "AccessLevelId", "dbo.AccessLevels");
            DropIndex("dbo.XrayPhotoes", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "AccessLevelId" });
            DropIndex("dbo.Users", new[] { "PositionId" });
            DropTable("dbo.XrayPhotoes");
            DropTable("dbo.Users");
            DropTable("dbo.Positions");
            DropTable("dbo.AccessLevels");
        }
    }
}
