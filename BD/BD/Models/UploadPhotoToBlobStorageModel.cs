﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BD.Models
{
    public class UploadPhotoToBlobStorageModel
    {
        public byte[] PhotoContent { get; set; }

        public string FileName { get; set; }

    }
}