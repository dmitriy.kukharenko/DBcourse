﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BD.Entities;
using BD.Services;
using System.IO;
using System.Configuration;

namespace BD.Controllers
{
    public class XrayPhotoesController : Controller
    {
        private XrayDbContext db = new XrayDbContext();
        private BlobStorageProvider blobLoader = new BlobStorageProvider();

        public XrayPhotoesController()
        {

        }

        // GET: XrayPhotoes
        public ActionResult Index()
        {
            var xrayPhotos = db.XrayPhotos.Include(x => x.User);
            return View(xrayPhotos.ToList());
        }

        // GET: XrayPhotoes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            XrayPhoto xrayPhoto = db.XrayPhotos.Find(id);
            if (xrayPhoto == null)
            {
                return HttpNotFound();
            }
            xrayPhoto.PhotoLink = blobLoader.GetPhotoUrl(xrayPhoto.PhotoLink);
            return View(xrayPhoto);
        }

        // GET: XrayPhotoes/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "Id", "Username");
            return View();
        }

        // POST: XrayPhotoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PhotoLink,Description,DateLoaded,UserId")] XrayPhoto xrayPhoto)
        {
            xrayPhoto.DateLoaded = DateTime.UtcNow.Date;
            if (ModelState.IsValid)
            {
                xrayPhoto.Id = Guid.NewGuid();
                db.XrayPhotos.Add(xrayPhoto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "Id", "Username", xrayPhoto.UserId);
            return View(xrayPhoto);
        }

        // GET: XrayPhotoes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            XrayPhoto xrayPhoto = db.XrayPhotos.Find(id);
            if (xrayPhoto == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "Username", xrayPhoto.UserId);
            return View(xrayPhoto);
        }

        // POST: XrayPhotoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PhotoLink,Description,DateLoaded,UserId")] XrayPhoto xrayPhoto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(xrayPhoto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "Username", xrayPhoto.UserId);
            return View(xrayPhoto);
        }

        // GET: XrayPhotoes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            XrayPhoto xrayPhoto = db.XrayPhotos.Find(id);
            if (xrayPhoto == null)
            {
                return HttpNotFound();
            }
            return View(xrayPhoto);
        }

        // POST: XrayPhotoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            XrayPhoto xrayPhoto = db.XrayPhotos.Find(id);
            db.XrayPhotos.Remove(xrayPhoto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult LoadPhoto()
        {
            string fileName = "";
            try
            {
                
                foreach(string file in Request.Files)
                {
                    HttpPostedFileBase fileBase = Request.Files[file];
                    fileName = fileBase.FileName;
                    if (fileBase!=null && fileBase.ContentLength>0)
                    {
                        byte[] buffer = new byte[fileBase.ContentLength+1];
                        var fileStream = new MemoryStream();
                        fileBase.InputStream.Read(buffer,0,fileBase.ContentLength);
                        var fs = new FileStream(@"D:\test.jpg", FileMode.OpenOrCreate);
                        fileBase.InputStream.CopyTo(fs);
                        var newName = Guid.NewGuid().ToString()+"." + fileBase.FileName.Split('.').LastOrDefault();
                        var result = blobLoader.UploadPhoto(new Models.UploadPhotoToBlobStorageModel
                        {
                            FileName = newName,
                            PhotoContent = buffer
                        });
                        fs.Close();
                        return Json(new { result});
                    }

                    return Json(new { });
                }
                return Json(new { });
            }
            catch (Exception exception)
            {
                return Json(new { exception });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
