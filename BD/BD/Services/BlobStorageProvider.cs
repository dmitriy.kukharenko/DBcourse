﻿using BD.Models;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BD.Services
{
    public class BlobStorageProvider
    {
        private readonly CloudBlobClient _blobClient;
        private readonly CloudBlobContainer _container;
        public BlobStorageProvider()
        {
            var storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageAccountConnection"));
            _blobClient = storageAccount.CreateCloudBlobClient();
            _container = _blobClient.GetContainerReference("blobs");
        }

        public string UploadPhoto(UploadPhotoToBlobStorageModel model)
        {
            var blob = _container.GetBlockBlobReference(model.FileName);
            blob.UploadFromByteArray(model.PhotoContent, 0, model.PhotoContent.Length - 1);

            return model.FileName;
        }

        public void DeletePhoto(string name)
        {
            var blob = _container.GetBlockBlobReference(name);
            blob.Delete();
        }


        public byte[] DownloadPhoto(string name)
        {
            var blob = _container.GetBlockBlobReference(name);
            using (var memstream = new MemoryStream())
            {
                blob.DownloadToStream(memstream);
                return memstream.ToArray();
            }
        }

        public string GetPhotoUrl(string name)
        {
            var blob = _container.GetBlockBlobReference(name);
            return blob.Uri.AbsoluteUri;
        }
    }
}