﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BD.Entities
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }

        public Guid PositionId { get; set; }

        public int AccessLevelId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public DateTime DateRegistered { get; set; }

        public DateTime DateUpdated { get; set; }

        public virtual Position Position { get; set; }

        public virtual AccessLevel AccessLevel { get; set; }

        public virtual List<XrayPhoto> XrayPhotos { get; set; }
    }
}