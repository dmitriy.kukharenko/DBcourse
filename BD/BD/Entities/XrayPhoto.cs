﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BD.Entities
{
    public class XrayPhoto
    {
        [Key]
        public Guid Id { get; set; }

        public string PhotoLink { get; set; }

        public string Description { get; set; }

        public DateTime DateLoaded { get; set; }

        public Guid UserId { get; set; }

        public virtual User User { get; set; }
    }
}