﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BD.Entities
{
    public class XrayDbContext: DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<AccessLevel> AccessLevels { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<XrayPhoto> XrayPhotos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasRequired(x => x.Position);
            modelBuilder.Entity<User>().HasRequired(x => x.AccessLevel);
            modelBuilder.Entity<User>().HasMany(x => x.XrayPhotos).WithRequired(x => x.User).WillCascadeOnDelete(true);
        }
    }
}